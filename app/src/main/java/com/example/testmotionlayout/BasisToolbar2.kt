package com.example.testmotionlayout

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View.OnFocusChangeListener
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mindorks.editdrawabletext.EditDrawableText


class BasisToolbar2 : AppCompatActivity() {

    val animals: ArrayList<String> = ArrayList()
    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basis_toolbar2)


        val edittext = findViewById(R.id.et_search) as? EditDrawableText
        edittext?.setOnFocusChangeListener(OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) { //Do your work
                edittext.performClick()
            }
        })

        addAnimals()

        val  recyclerView = findViewById(R.id.recyclerview2) as? RecyclerView

        addAnimals()
        recyclerView?.layoutManager = LinearLayoutManager(this,
            LinearLayout.VERTICAL, false)

        val adapter = AnimalAdapter(animals)

        recyclerView?.adapter = adapter
    }

    fun addAnimals() {

        animals.add("dog")
        animals.add("cat")
        animals.add("owl")
        animals.add("cheetah")
        animals.add("raccoon")
        animals.add("bird")
        animals.add("snake")
        animals.add("lizard")
        animals.add("hamster")
        animals.add("bear")
        animals.add("lion")
        animals.add("tiger")
        animals.add("horse")
        animals.add("frog")
        animals.add("fish")
        animals.add("shark")
        animals.add("turtle")
        animals.add("elephant")
        animals.add("cow")
        animals.add("beaver")
        animals.add("bison")
        animals.add("porcupine")
        animals.add("rat")
        animals.add("mouse")
        animals.add("goose")
        animals.add("deer")
        animals.add("fox")
        animals.add("moose")
        animals.add("buffalo")
        animals.add("monkey")
        animals.add("penguin")
        animals.add("parrot")
    }
}
