package com.example.testmotionlayout

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AnimalAdapter(val animalList: ArrayList<String>): RecyclerView.Adapter<AnimalAdapter.ListRecyclerViewHolder>() {



    class ListRecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textView: TextView? = null
        init {
                   textView = itemView.findViewById(R.id.text)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListRecyclerViewHolder {

        val inflator: LayoutInflater = LayoutInflater.from(parent.context)

        val view: View = inflator.inflate(R.layout.recyclerview_item, parent, false);

        return ListRecyclerViewHolder(view)
    }

    override fun getItemCount(): Int {
       return animalList.size
    }

    override fun onBindViewHolder(holder: ListRecyclerViewHolder, position: Int) {

        holder.textView?.text = animalList[position]
    }
}